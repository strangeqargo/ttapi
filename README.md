## TЗ
https://docs.google.com/document/d/12dEq2_lhTcJDywFwNm3TOP8p-I1MwZwkce1tEonz27M/edit?usp=sharing
### запуск
`docker-compose up`
`docker-compose exec php bin/console doctrine:migrations:migrate`

### проверка
`https://localhost/docs` : 
параметры запроса: 

curl:

```
curl -X POST "https://localhost/users/register" -H  "accept: application/ld+json" -H  "Content-Type: application/ld+json" -d "{\"phoneNumber\":\"string1111122\",\"code\":\"str43423ing\"}"
```

json: 

```
{
  "phoneNumber": "string1111122",
  "code": "str43423ing"
}
```

весь этот чудесный код, естественно лежит в src/, генерация токена в src/Service/Token, остальное в Controller/User и Entity/User

при попытке созданя дубликата пользователя с существующим телефоном бд будет ругаться.