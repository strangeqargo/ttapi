<?php
namespace App\Service;
/*
 фейковый сервис генерации токенов
*/

class Token
{
   
    public  static function RandomString(int $length): string {
   	$keys = array_merge(range(0,9), range('a', 'z'));

		$key = "";
		for($i=0; $i < $length; $i++) {
			$key .= $keys[mt_rand(0, count($keys) - 1)];
		}
		return $key;
	}

	public static function getToken(string $authCode): string{
		 return self::RandomString(20);
  	}
   
    
    
}