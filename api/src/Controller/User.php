<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Token;
use App\Entity\User as UserEntity;
use Symfony\Component\Validator\Constraints as Assert;

class User {

  // private UserEntity $user;
  
  // public function __construct(int $id = 0, string $phone="", string $code=""){
  //   $this->user = new UserEntity(0, $phone);
  // }
  // наверное это можно все обрабатывать в __invoke

  public function getToken($authCode)
  {
      return Token::getToken($authCode);
  }

  public function __invoke($data): UserEntity{
    //$x = strlen($data->getPhoneNumber()); //just a test
    return $data;

  }

}