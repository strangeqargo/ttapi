<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\User as UserController;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 *  User entity
 *
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @ApiResource(
 * 
 * collectionOperations={
 *     "get",
 *     "register_action"={
 *          "method"="POST",
 *          "path"="/users/register",
 *          "controller"=UserController::class,
 *          "read"= false
 *     }
 *  
 * }
 * 
 * )
 */
class User
{
    /**
     * The entity ID
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @ApiProperty(identifier=true)
     */
    private ?int $id = null;

    /**
     * Phone Number
     *
     * @ORM\Column(name="phone", type="string", length=64, unique=true)
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private string $phoneNumber;

    private string $code = "";
    private string $token = "";
        

    public function setCode(string $code){
        $this->code = $code;
    }

    public function getToken(){
         $userController = new UserController();
         return $userController->getToken($this->code);
         
    }

    public function getId(): ?int
    {
        return $this->id;
	}
	
	public function getPhoneNumber(): ?string {
		return $this->phoneNumber;
	}
	
    public function setPhoneNumber($phoneNumber)
    {
		$this->phoneNumber = $phoneNumber;
    }
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('phoneNumber', new Assert\NotBlank());
        $metadata->addPropertyConstraint('code', new Assert\NotBlank());
    }
    
}
